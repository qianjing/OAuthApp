﻿using System.Collections.Generic;

namespace OAuthApp.ApiModels.AppsController
{
    public class LogReportResponse
    {
        public string Api { get; set; }

        public string Items { get; set; }

        public long Users { get; set; }

        public long Total { get; set; }

    }
}
