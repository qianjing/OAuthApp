# OAuthApp

#### 介绍

H5应用发布工具。[立即访问](https://www.oauthapp.com/tenant/index)  (请使用Chrome访问)

#### 软件架构
- .Net Core 6.0
- Angular 13



#### 功能：

- 1，在线发布HTML应用，支持版本回滚、应用复制、在线管理等功能。
- 2，可使用的功能：用户管理、数据库、排行榜。[开发文档](https://web.oauthapp.com/4/docs/)
- 3，代码生成服务：支持写代码方式/上传zip压缩包方式，生成代码，在线部署。[制作文档](https://web.oauthapp.com/4/docs/master/)


#### 部分截图预览

![0](jietu/20220712_0.png)

![1](jietu/20220712_1.png)

![2](jietu/20220712_2.png)

![3](jietu/20220712_3.png)

![4](jietu/20220712_4.png)

![5](jietu/20220712_5.png)

#### 技术交流微信，添加时备注：oauthapp
![技术交流微信](jietu/20220906_1.png.jpg)