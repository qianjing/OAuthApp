﻿namespace OAuthApp.ApiModels.AppsController
{
    public class UploadFileRequest
    {
        public long serverID { get; set; }

        public string path { get; set; }

        public string content { get; set; }
    }
}
