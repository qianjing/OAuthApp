﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace OAuthApp.Services
{
    public class UploadService
    {
        private readonly IWebHostEnvironment _env;

        public UploadService(IWebHostEnvironment env)
        {
            _env = env;
        }

        public bool Upload(string savePath, IFormFile file)
        {
            var path = Path.Combine(_env.WebRootPath, "blobs", savePath);

            var fileDirectory = Path.GetDirectoryName(path);

            if (!Directory.Exists(fileDirectory))
            {
                Directory.CreateDirectory(fileDirectory);
            }

            using var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);

            file.CopyTo(fs);

            return true;
        }

        public (bool,long) CopyTo(string fromPath,string savePath)
        {
            var path = Path.Combine(_env.WebRootPath, "blobs", savePath);

            var fileDirectory = Path.GetDirectoryName(path);

            if (!Directory.Exists(fileDirectory))
            {
                Directory.CreateDirectory(fileDirectory);
            }

            var fs = new FileInfo(fromPath);

            fs.CopyTo(path, true);

            return (true, fs.Length);
        }
    }
}
