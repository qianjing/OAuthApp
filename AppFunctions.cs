﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OAuthApp
{
    public static class AppFunctions
    {
        public static string md5(string str)
        {
            var bt = Encoding.UTF8.GetBytes(str);

            var md5 = MD5.Create();

            var md5bt = md5.ComputeHash(bt);

            StringBuilder builder = new StringBuilder();

            foreach (var item in md5bt)
            {
                builder.Append(item.ToString("x2"));
            }

            var md5Str = builder.ToString();

            return builder.ToString();
        }

        public static bool md5Compare(string pwd,string pwd2)
        {
            var pwd_md5 = md5(pwd);

            return pwd_md5 == pwd2;
        }
    }
}
