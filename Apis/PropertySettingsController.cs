﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OAuthApp.Data;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System;
using OAuthApp.Filters;
using OAuthApp.Services;
using Microsoft.AspNetCore.Authorization;

namespace OAuthApp.Apis
{
    [SwaggerTag("应用属性")]
    [ServiceFilter(typeof(ApiRequestLoggingAttribute))]
    public class PropertySettingsController : BaseController
    {
        private readonly AppDbContext _context;

        public PropertySettingsController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [SwaggerOperation(OperationId = "PropertySettings")]
        [EncryptResultFilter]
        public IActionResult List([Required]string channelCode,long channelAppId)
        {
            var result = _context.PropertySettings
                .Where(x => x.ChannelCode == channelCode && x.ChannelAppId == channelAppId).ToList();

            return OK(result);
        }

        [HttpPut("{id}")]
        [SwaggerOperation(OperationId = "PropertySettingPut")]
        public IActionResult Put(long id, PropertySetting appProperty)
        {
            var item = _context.PropertySettings
               .FirstOrDefault(x => x.ID == appProperty.ID);

            if (item == null)
            {
                return Error("不存在的属性");
            }

            try
            {
                item.Value = appProperty.Value;
                item.Tag = appProperty.Tag;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }

            return OK(true);
        }

        [HttpPost]
        [SwaggerOperation(OperationId = "PropertySettingPost")]
        public IActionResult Post(PropertySetting appProperty)
        {
            if (_context.PropertySettings.Any(x =>
             x.ChannelCode == appProperty.ChannelCode &&
             x.ChannelAppId == appProperty.ChannelAppId &&
             x.Name == appProperty.Name))
            {
                return Error("已存在的属性");
            }

            _context.PropertySettings.Add(appProperty);

            _context.SaveChanges();

            return OK(new { id = appProperty.ID });
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = "PropertySettingDelete")]
        public IActionResult Delete(long id)
        {
            var appProperty = _context.PropertySettings.FirstOrDefault(x => x.ID == id);

            if (appProperty == null)
            {
                return NotFound();
            }

            _context.PropertySettings.Remove(appProperty);

            _context.SaveChanges();

            return OK(true);
        }

        // 免验证获取应用配置信息
        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = "PropertySetting")]
        [AllowAnonymous]
        public IActionResult Detail(long id, string tag, string name)
        {
            var result = _context.PropertySettings
                .Where(x => x.ChannelCode.Equals(ChannelCodes.App)
                && x.ChannelAppId == id
                && x.Tag.Equals(tag)
                && x.Name.Equals(name))
                .Select(x => new { id = x.ID, settings = x.Value })
                .FirstOrDefault();

            if (result == null)
            {
                return Error("不存在的配置");
            }

            return OK(result);
        }
    }
}
