﻿namespace OAuthApp.ApiModels.AppsController
{
    public class DeleteFileRequest
    {
        public long serverID { get; set; }

        public string path { get; set; }

        public long fileSize { get; set; }
    }
}
