﻿using System;
namespace OAuthApp.ApiModels.AppUsersController
{
    public class QRCodePreSignInRequest
    {
        public long AppID { get; set; }

        public string Scopes { get; set; }

        public string Remark { get; set; }
    }
}
