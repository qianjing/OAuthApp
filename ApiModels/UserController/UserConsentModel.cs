﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OAuthApp.ApiModels.UserController
{
    public class UserConsentModel
    {
        public long ID { get; set; }

        public string AppKey { get; set; }

        public string AppName { get; set; }

        public string AppLogo { get; set; }

        public string Scope { get; set; }

        public string RedirectUri { get; set; }

        public string ResponseType { get; set; }

        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
