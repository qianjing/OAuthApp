﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OAuthApp.ApiModels.AppsController
{
    public class WebProxyRequest
    {
        /// <summary>
        /// 请求方式
        /// </summary>
        [Required]
        public string method { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        [Required]
        public string uri { get; set; }

        /// <summary>
        /// 提交的数据
        /// </summary>
        public string jsonData { get; set; }

        public List<string> headerNames { get; set; }

        public List<string> headerValues { get; set; }
    }
}
