﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OAuthApp.ApiModels.AppsController
{
    public class CreateDirectoryRequest
    {
        public long serverID { get; set; }

        public string path { get; set; }
    }
}
