﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OAuthApp.Tenant;
using Swashbuckle.AspNetCore.Annotations;
using System;
using Microsoft.AspNetCore.Http;
using OAuthApp.Filters;
using OAuthApp.Services;

namespace OAuthApp.Apis
{
    [SwaggerTag("租户属性")]
    [ServiceFilter(typeof(ApiRequestLoggingAttribute))]
    public class TenantPropertiesController : BaseController
    {
        public TenantPropertiesController(TenantDbContext context,
            IHttpContextAccessor contextAccessor)
        {
            _tenantContext = context;
            _tenant = contextAccessor.HttpContext.GetTenantContext();
        }

        [HttpGet]
        [SwaggerOperation(OperationId = "TenantProperties")]
        [EncryptResultFilter]
        public IActionResult List()
        {
            var result = _tenantContext.TenantProperties
                .Where(x => x.TenantID == _tenant.Id)
                .OrderByDescending(x => x.ID)
                .ToList();

            return OK(result);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = "TenantProperty")]
        [EncryptResultFilter]
        public IActionResult Get(long id)
        {
            var result = _tenantContext.TenantProperties
                .FirstOrDefault(x => x.ID == id);

            if (result == null)
            {
                return NotFound();
            }

            return OK(result);
        }

        [HttpPut("{id}")]
        [SwaggerOperation(OperationId = "TenantPropertyPut")]
        public IActionResult Put(TenantProperty tenantProperty)
        {
            var item = _tenantContext.TenantProperties
                .FirstOrDefault(x => x.ID == tenantProperty.ID);

            if (item == null)
            {
                return Error("不存在的属性");
            }

            try
            {
                item.Value = tenantProperty.Value;
                _tenantContext.SaveChanges();
            }

            catch (Exception ex)
            {
                return Error(ex.Message);
            }

            return OK(true);
        }

        [HttpPost]
        [SwaggerOperation(OperationId = "TenantPropertyPost")]
        public IActionResult Post(TenantProperty tenantProperty)
        {
            if(_tenantContext.TenantProperties.Any(
                x=>x.TenantID==tenantProperty.TenantID&&
                x.Name==tenantProperty.Name))
            {
                return Error("已存在的属性");
            }

            _tenantContext.TenantProperties.Add(tenantProperty);

            _tenantContext.SaveChanges();

            return OK(new { id = tenantProperty.ID });
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = "TenantPropertyDelete")]
        public IActionResult Delete(long id)
        {
            var result = _tenantContext.TenantProperties
                .FirstOrDefault(x => x.ID == id);

            if (result == null)
            {
                return NotFound();
            }

            _tenantContext.TenantProperties.Remove(result);

            _tenantContext.SaveChanges();

            return OK(true);
        }
    }
}
