﻿using System.Collections.Generic;

namespace OAuthApp.Tenant
{
    public class TenantUserModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        public List<string> Role { get; set; } = new List<string>();

        public string Avatar { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
    }
}
