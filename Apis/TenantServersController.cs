﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using OAuthApp.ApiModels.TenantServersController;
using OAuthApp.Tenant;
using Microsoft.AspNetCore.Http;
using OAuthApp.Filters;
using System;
using Microsoft.EntityFrameworkCore;
using OAuthApp.Services;

namespace OAuthApp.Apis
{
    [SwaggerTag("服务器")]
    [ServiceFilter(typeof(ApiRequestLoggingAttribute))]
    public class TenantServersController : BaseController
    {
        public TenantServersController(TenantDbContext context,
            IHttpContextAccessor contextAccessor)
        {
            _tenantContext = context;
            _tenant = contextAccessor.HttpContext.GetTenantContext();
        }

        [HttpGet("Market")]
        [SwaggerOperation(OperationId = "TenantServerMarket")]
        [EncryptResultFilter]
        public IActionResult Market(string tag)
        {
            var where = " Tag ='site,public' OR UserID = " + UserID;

            var cmd = @"SELECT * FROM TenantServers WHERE (" + where + ") " +
                " AND (TenantID = 0 OR TenantID = @TenantID) ORDER BY Sort DESC, ID DESC";          

            var result = _tenantContext.Query<MarketResponse>(cmd, new
            {
                TenantID = _tenant.Id
            });

            return OK(result);
        }

        [HttpGet]
        [SwaggerOperation(OperationId = "TenantServers")]
        [EncryptResultFilter]
        public IActionResult List()
        {
            var result = _tenantContext.TenantServers.Where(x =>
            x.TenantID == _tenant.Id &&
            x.UserID == UserID)
                .OrderByDescending(x => x.Sort)
                .OrderByDescending(x => x.ID).ToList();

            return OK(result);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = "TenantServer")]
        [AllowAnonymous]
        [EncryptResultFilter]
        public IActionResult Get(long id)
        {
            var result = _tenantContext.TenantServers
                .FirstOrDefault(x => x.ID == id && !x.IsDelete && x.UserID == UserID);

            if (result == null || result.TenantID < 1)
            {
                return Error("未找到数据或权限不足。");
            }

            return OK(result);
        }

        [HttpPut("{id}")]
        [SwaggerOperation(OperationId = "TenantServerPut")]
        public IActionResult Put(long id, TenantServer tenantServer)
        {
            if (id != tenantServer.ID ||
                !_tenantContext.TenantServers.Any(x => x.TenantID == _tenant.Id && x.UserID == UserID))
            {
                return NotFound();
            }

            tenantServer.TenantID = _tenant.Id;
            tenantServer.UserID = UserID;

            _tenantContext.Entry(tenantServer).State = EntityState.Modified;

            try
            {
                _tenantContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }

            return OK(true);
        }

        [HttpPost]
        [SwaggerOperation(OperationId = "TenantServerPost")]
        public IActionResult Post(TenantServer tenantServer)
        {
            tenantServer.TenantID = _tenant.Id;

            tenantServer.UserID = UserID;

            _tenantContext.TenantServers.Add(tenantServer);

            _tenantContext.SaveChanges();

            return OK(new { id = tenantServer.ID });
        }

        //[HttpDelete("{id}")]
        //[SwaggerOperation(OperationId = "TenantServerDelete")]
        //public IActionResult Delete(long id)
        //{
        //    var result = _context.TenantServers
        //        .FirstOrDefault(x => x.ID == id && x.UserID == UserID && !x.IsDelete);

        //    if (result == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.TenantServers.Remove(result);

        //    _context.SaveChanges();

        //    return OK(true);
        //}
    }
}
