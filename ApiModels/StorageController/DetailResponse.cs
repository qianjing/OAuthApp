﻿namespace OAuthApp.ApiModels.StorageController
{
    public class DetailResponse
    {
        public long ID { get; set; }
        public string Content { get; set; }
        public string LastChange { get; set; }
    }
}
