﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OAuthApp.Models
{
    public class SignInIdentityModel
    {
        public List<string> Role { get; set; }

        public string Avatar { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }


    }
}
