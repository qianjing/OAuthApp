﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OAuthApp.Data.AppMigrations
{
    public partial class _20220309UpdateAppDbMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OpenID",
                table: "AppChatMessages",
                newName: "UnionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UnionID",
                table: "AppChatMessages",
                newName: "OpenID");
        }
    }
}
